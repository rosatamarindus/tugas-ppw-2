from django.apps import AppConfig


class HalStatusConfig(AppConfig):
    name = 'hal_status'
