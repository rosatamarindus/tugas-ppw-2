from django.db import models

# Create your models here.
class Pengguna(models.Model):
	kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True)
	nama = models.CharField('Nama', max_length=100)
	created_date_user= models.DateTimeField(auto_now_add=True)
	updated_date_user = models.DateTimeField(auto_now_add=True) 

class Status(models.Model):
	pengguna = models.ForeignKey(Pengguna)
	description = models.TextField()
	created_date = models.DateTimeField(auto_now_add=True)

class Comment(models.Model):
	pengguna = models.ForeignKey(Status,on_delete=models.CASCADE)
	comment = models.CharField(max_length=400)
	created_date = models.DateTimeField(auto_now_add=True)