[![pipeline status](https://gitlab.com/rosatamarindus/tugas-ppw-2/badges/master/pipeline.svg)](https://gitlab.com/rosatamarindus/tugas-ppw-2/commits/master)
[![coverage report](https://gitlab.com/rosatamarindus/tugas-ppw-2/badges/master/coverage.svg)](https://gitlab.com/rosatamarindus/tugas-ppw-2/commits/master)
## Gitlab repo link:
[https://gitlab.com/rosatamarindus/tugas-ppw-2](https://gitlab.com/rosatamarindus/tugas-2-ppw)
## Heroku app link:
[https://tugaskeduappw.herokuapp.com/](https://tugaskeduappw.herokuapp.com/)

## Kelompok 11 -- Kelas PPW-D:

1. [X] Rosa Nur Rizky Febrianty Tamarindus - 1606917916 - R
2. [X] M. Hanif Pratama - 1606876065 - MHP
3. [X] Farolina Rahmatunnisa -1606827870 - F
4. [ ] Syifa Salsabila R
